function asociar_hijo(obj_padre,obj_hijo){
    obj_padre.appendChild(obj_hijo);
}


function crear_label(nom,val){
    obj=document.createElement("label");
    obj.setAttribute("name",nom);
    obj.innerHTML = val;
    return obj;
}

function crear_input_text(nom,val){
    obj = document.createElement("input");
    obj.setAttribute("type","text");
    obj.setAttribute("name",nom);
    obj.setAttribute("value",val);
    return obj;
}

function crear_input_email(nom,value){
    obj = document.createElement("input");
    obj.setAttribute("type","email");
    obj.setAttribute("name",nom);
    obj.setAttribute("value",value);
    return obj;
}

function crear_textArea(nom,val){
    obj = document.createElement("textarea");
    obj.setAttribute("name",nom);
    obj.innerHTML = val;
    return obj;
}

function crear_input_contrasenia(nom,val){
    obj = document.createElement("input");
    obj.setAttribute("type","password");
    obj.setAttribute("name",nom);
    obj.setAttribute("placeholder",val);
    return obj;
}

function crear_radio(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","radio");
    obj.setAttribute("name",nom);
    return obj;
}

function crear_radio_check(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","radio checked");
    obj.setAttribute("name",nom);
    return obj;
}


function crear_check(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","checkbox");
    obj.setAttribute("name",nom);
    return obj;
}

function crear_option(nom,opciones){
    obj = document.createElement("select");
    obj.setAttribute("name",nom);
    for(var i=0;i < opciones.length;i++){
        obj_o = document.createElement("option");
        obj_o.innerHTML = opciones[i];
        asociar_hijo(obj,obj_o);
    }
    return obj;
}

function crear_option_gr(nom,opciones,opciones2){
    obj = document.createElement("select");
    obj.setAttribute("name",nom);
    gro1 = document.createElement("optgroup");
    gro1.setAttribute("label","Grupo 1");
    asociar_hijo(obj,gro1);
    for(var i=0;i < opciones.length;i++){
        obj_o = document.createElement("option");
        obj_o.innerHTML = opciones[i];
        asociar_hijo(gro1,obj_o);
    }
    gro2 = document.createElement("optgroup");
    gro2.setAttribute("label","Grupo 2");
    asociar_hijo(obj,gro2);
    for(var i=0;i < opciones2.length;i++){
        obj_o = document.createElement("option");
        obj_o.innerHTML = opciones2[i];
        asociar_hijo(gro2,obj_o);
    }
    return obj;
}

function crear_file(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","file");
    obj.setAttribute("name",nom);
    return obj;
}


function crear_fieldset(nom){
    obj = document.createElement("fieldset");
    obj.setAttribute("name",nom)
    leg = document.createElement("legend");
    leg.innerHTML = "Legend";
    asociar_hijo(obj,leg);
    lab=document.createElement("label");
    lab.innerHTML="Label";
    lab.setAttribute("for","field");
    asociar_hijo(obj,lab);
    inp = document.createElement("input");
    inp.setAttribute("id","field");
    asociar_hijo(obj,inp);
    return obj;
}


function crear_input_number(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","number");
    obj.setAttribute("name",nom);
    return obj;
}

function crear_input_number_min_max(nom,min,max,step){
    obj = document.createElement("input");
    obj.setAttribute("type","number");
    obj.setAttribute("name",nom);
    obj.setAttribute("min",min);
    obj.setAttribute("max",max);
    obj.setAttribute("step",step);
    return obj;
}

function crear_input_range(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","range");
    obj.setAttribute("name",nom);
    return obj;
}


function crear_input_range_min_max(nom,min,max,step){
    obj = document.createElement("input");
    obj.setAttribute("type","range");
    obj.setAttribute("name",nom);
    obj.setAttribute("min",min);
    obj.setAttribute("max",max);
    obj.setAttribute("step",step);
    return obj;
}

function crear_input_color(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","color");
    obj.setAttribute("name",nom);
    return obj;
}

function crear_input_color_val(nom,val){
    obj = document.createElement("input");
    obj.setAttribute("type","color");
    obj.setAttribute("name",nom);
    obj.setAttribute("value",val);
    return obj;
}

function crear_progreso_val(nom,max){
    obj = document.createElement("progress");
    obj.setAttribute("nom",nom);
    obj.setAttribute("max",max);
    obj.setAttribute("value",50);
    return obj
}

function crear_progreso(nom){
    obj = document.createElement("progress");
    obj.setAttribute("nom",nom);
    return obj
}


var la1=crear_label("la1","Ejemplo de label");
var la2=crear_label("la2","Ejemplo de input text");
var la3=crear_label("la3","Ejemplo de input email");
var la4=crear_label("la4","Ejemplo de input contraseña");
var la5=crear_label("la5","Ejemplo de TextArea");
var la6=crear_label("la6","Ejemplo de RadioButton");
var la8=crear_label("la8","Ejemplo de Check");
var la9=crear_label("la9","Ejemplo de opciones");
var la10=crear_label("la10","Ejemplo de opciones con grupos");
var la11=crear_label("la11","Ejemplo de file");
var la12=crear_label("la12","Ejemplo de field set");
var la13=crear_label("la13","Ejemplo de Input number");
var la14=crear_label("la14","Ejemplo de input number con max y min");
var la15=crear_label("la15","Ejemplo de input range");
var la16=crear_label("la16","Ejemplo de input range 2");
var la17=crear_label("la17","Ejemplo de color");
var la18=crear_label("la18","Ejemplo de color cambiante");
var la19=crear_label("la19","Ejemplo de progress");
var la20=crear_label("la20","Ejemplo de progress 2");

var obj1=crear_label("label","Label...")
var obj2=crear_input_text("it","Soy un input :D");
var obj3=crear_input_email("ie","Soy para email :/");
var obj4=crear_input_contrasenia("ip","Soy para password");
var obj5=crear_textArea("ta","Soy para que escribas mucho texto :)");
var obj6=crear_radio("radio");
var obj8 = crear_check("check");
var obj9 = crear_option("opciones",["a","b","c","d"]);
var obj10 = crear_option_gr("Grupos",["a","b","c","d"],[1,2,3,4,5]);
var obj11 = crear_file("file");
var obj12 = crear_fieldset("field set");
var obj13 = crear_input_number("input number");
var obj14 = crear_input_number_min_max("Max",0,100,10);
var obj15 = crear_input_range("input range");
var obj16 = crear_input_range_min_max("rango2",0,100,15);
var obj17 = crear_input_color("color");
var obj18 = crear_input_color_val("color 2","#e76252");
var obj19 = crear_progreso_val("progreso 2",100);
var obj20 = crear_progreso_val("progreso",100);
arregloLabels=[la1,la2,la3,la4,la5,la6,la8,la9,la10,la11,la12,la13,la14,la15,la16,la17,la18,la19,la20];
arregloObj=[obj1,obj2,obj3,obj4,obj5,obj6,obj8,obj9,obj10,obj11,obj12,obj13,obj14,obj15,obj16,obj17,obj18,obj19,obj20];




function formulario(label,obj){

var titulo = document.createElement("h1");
titulo.setAttribute("align","center");
titulo.innerHTML="Generando formulario dinamico";
asociar_hijo(document.body,titulo);

    var tabla=document.createElement("form");
    tabla.setAttribute("role","form");
    
     for(var i=0; i<obj.length;i++){
        var div2=document.createElement("div");
        div2.setAttribute("class","col-md-4");
        asociar_hijo(div2,label[i]);
        asociar_hijo(tabla,div2);

         
        var div = document.createElement("div");
        div.setAttribute("class","col-md-6");
        obj[i].setAttribute("class","form-control");
        asociar_hijo(div,obj[i]);
        asociar_hijo(tabla,div);
    }
    asociar_hijo(document.body,tabla);
}

formulario(arregloLabels,arregloObj);

